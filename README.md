# duoslive

This is an [Archiso](https://wiki.archlinux.org/index.php/Archiso) profile for building a DUOS live image. You can find prebuilt images in the [DUOS Releases](https://github.com/LadueCS/duos/releases).

To build an image, you will need to have Archiso installed either in an existing Arch or DUOS installation.

The following command will generate a live image:

```sh
sudo mkarchiso -v .
```

If you have plenty of free memory, this command will build the image in a RAM disk, which is much faster:

```sh
sudo mkarchiso -v -w /tmp/archiso-tmp .
sudo rm /tmp/archise-tmp -rf # Clean up environment afterwards
```

You will find the live image in the `out` directory.

